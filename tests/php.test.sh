#!/bin/sh

ret=$(docker run -d --rm --name $test_container_name $test_image)

log 5 "container id $ret"

sleep 2

ret=$(docker logs $test_container_name 2>&1 | grep "ready to handle connections")

if [ "$?" != "0" ] || [ -z "$ret" ] ; then
  log 3 "testing image not successfull!"
  exit 1
fi

log 7 "testing image successfull $ret"
