ARG BASE_IMAGE=
ARG BASE_TAG=
ARG HTTP_PROXY=
ARG HTTPS_PROXY=
ARG http_proxy=
ARG https_proxy=

FROM ${BASE_IMAGE}:${BASE_TAG}

LABEL maintainer="Stefan Schneider <eqsoft4@gmail.com>"

USER root

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin
SHELL ["/bin/bash", "-c"]

ARG UID=33
ARG GID=33
ARG PHANTOMJS=phantomjs_2.1.1

RUN <<EOF
set -e
apt-get update
apt-get install -y --no-install-recommends \
tzdata \
apt-transport-https \
ca-certificates \
software-properties-common \
imagemagick \
libxslt1-dev \
zip \
unzip \
git \
libpng-dev \
openssl \
patch \
wget \
curl \
build-essential \
libmemcached-dev \
nano
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
echo $TZ > /etc/timezone
EOF

RUN <<EOF
set -e
apt-get update
curl -sSLf -o /usr/local/bin/install-php-extensions https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions
chmod +x /usr/local/bin/install-php-extensions
install-php-extensions \
apcu \
gd \
ldap \
memcache \
memcached \
mysqli \
pdo_mysql \
imagick \
soap \
xdebug \
xmlrpc \
xsl \
zip \
@composer
mkdir -p /var/www/data
chown -R "$UID:$GID" /var/www/data
chown -R "$UID:$GID" /var/www/html
EOF

COPY 01-custom.ini /usr/local/etc/php/conf.d

# for documentation
VOLUME /var/www/html
VOLUME /var/www/data

EXPOSE 9000

# static phantomjs 2.1.1
COPY $PHANTOMJS /usr/bin/phantomjs
RUN chmod 755 /usr/bin/phantomjs

# copy ca certs
COPY crt/ /usr/share/ca-certificates/
COPY crt/ /usr/local/share/ca-certificates/
RUN <<EOF
set -e
echo root-ca.crt >> /etc/ca-certificates.conf
echo signing-ca.crt >> /etc/ca-certificates.conf
update-ca-certificates
EOF

USER "$UID:$GID"
